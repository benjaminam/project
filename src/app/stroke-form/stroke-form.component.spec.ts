import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrokeFormComponent } from './stroke-form.component';

describe('StrokeFormComponent', () => {
  let component: StrokeFormComponent;
  let fixture: ComponentFixture<StrokeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StrokeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StrokeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
