import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-stroke-form',
  templateUrl: './stroke-form.component.html',
  styleUrls: ['./stroke-form.component.css']
})
export class StrokeFormComponent implements OnInit {
  name:string;
  age: number;
  gender: number;
  hypertension: number;
  heart_disease: number;
  ever_married: number;
  work_type: number;
  Residence_type: number;
  avg_glucose_level: number;
  bmi: number;
  smoking_status: number;
  result: string;
  
  userId;
  data=[];
    predict(){
      this.data =  [this.age, this.gender, this.hypertension, this.heart_disease, this.ever_married, this.work_type, this.Residence_type
      	,this.avg_glucose_level, this.bmi,	this.smoking_status]
      this.predictService.predict(this.data).subscribe(
        res => {
          console.log(res);
          if (res>=0.5){
            this.result="High probability to stroke"
          }
          if (res<0.5) {
            this.result="Low probability to stroke"
          }
          // else {
          //   this.result="Please activate endpoint"
          // }
          }

      )
    }
    
    addStroke(){
      this.predictService.addStroke(this.name, this.userId, this.age, this.gender,this.hypertension, this.heart_disease, this.ever_married, this.work_type, this.Residence_type
      	,this.avg_glucose_level, this.bmi,	this.smoking_status,this.result)
      return this.router.navigate(['/stroke'])
    }
    cancel(){
      this.result='';
      
    }

    constructor(private router:Router,private predictService: PredictService, private authService:AuthService) { 
    }
  
    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user =>{
          this.userId = user.uid;
      
    })
  
  
   }
  }
  