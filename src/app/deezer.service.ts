import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Deezer } from './interfaces/deezer';

@Injectable({
  providedIn: 'root'
})
export class DeezerService {
  URL = 'https://api.deezer.com/user/205368755/flow';
  constructor(private http: HttpClient) { }

  getDeezer():Observable<Deezer>{
    // return this.http.get(this.URL); 
    return this.http.get<Deezer>(this.URL); 
    
  }}