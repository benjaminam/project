import { Stroke } from './../interfaces/stroke';
import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-stroke',
  templateUrl: './stroke.component.html',
  styleUrls: ['./stroke.component.css']
})
export class StrokeComponent implements OnInit {
    strokes$;
    strokes: Stroke[];
    email: string;
    userId:string;
    displayedColumns: string[] = ['name','age', 'gender', 'hypertension','avg_glucose_level','smoking_status','heart_disease','work_type','Residence_type','ever_married','bmi' , 'result' ,'delete'];
    dataSource = new MatTableDataSource<Stroke>() 

    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    genderDict = {0:'Female',1:'Male'};
    hypertensionDict = {0:'No',1:'Yes'};
    workDict= {0:'Private',1:'Self employed',2:'Governmental job',3:'Child',4:'Never'};
    residenceDict = {0:'Urban',1:'Rural'};
    smokeDict = {0:'Formerly',1:'Never',2:"Smoking"};
    marriedDict= {0:'No',1:'Yes'};
    heartDict= {0:'No',1:'Yes'};
  
  
  
    deleteStroke(index) {
      let id = this.strokes[index].id;
      this.predictService.deleteStroke(this.userId, id);
    }
  
   
  openform() {
    this.router.navigate(['/strokeForm']);
  }
 
  
    constructor(private router:Router, private predictService: PredictService, public authService: AuthService) { }
  
    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.email = user.email;
          this.userId=user.uid;
          console.log(this.email);
          this.strokes$ = this.predictService.getStroke(this.userId);
          this.strokes$.subscribe(
            docs => {
              this.strokes = [];
              var i = 0;
              for (let document of docs) {
                const stroke: Stroke = document.payload.doc.data();
                stroke.id = document.payload.doc.id;
                this.strokes.push(stroke);
              }
            }
          )
        }
      )

    }
  
  }
  