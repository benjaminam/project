import { DeezerComponent } from './deezer/deezer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoronaComponent } from './corona/corona.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StrokeComponent } from './stroke/stroke.component';
import { StrokeFormComponent } from './stroke-form/stroke-form.component';
import { RestaurantComponent } from './restaurant/restaurant.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'covid/:country', component: CoronaComponent},
  // { path: 'deezer', component: DeezerComponent},
  { path: 'stroke', component: StrokeComponent},
  { path: 'strokeForm', component: StrokeFormComponent},
  { path: 'restaurant', component: RestaurantComponent},




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
