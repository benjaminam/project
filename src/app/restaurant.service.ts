import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Restaurant } from './interfaces/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  constructor(private http: HttpClient) { }

URL = 'https://random-data-api.com/api/restaurant/random_restaurant';

getRestaurant(){
  return this.http.get<Restaurant>(this.URL); 
  
}}
