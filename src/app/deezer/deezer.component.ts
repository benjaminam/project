import { DeezerService } from './../deezer.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Deezer } from '../interfaces/deezer';

@Component({
  selector: 'deezer',
  templateUrl: './deezer.component.html',
  styleUrls: ['./deezer.component.css']
})
export class DeezerComponent implements OnInit {
  deezers;
  deezersData$:Observable<Deezer>;
  hasError:Boolean = false;
  errorMessage:string;
  title;
  // name;
  // duration;
  // artist;
  constructor(private DeezerService:DeezerService) { }

  ngOnInit(): void {
    this.deezersData$ = this.DeezerService.getDeezer();
    this.deezersData$.subscribe(
      data => {
        this.deezers = data;
        this.title=this.deezers.title;
        console.log(this.deezers);
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }

}
