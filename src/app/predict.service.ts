import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Stroke } from './interfaces/stroke';


@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://i5kjadvcb3.execute-api.us-east-1.amazonaws.com/beta";

  predict(data
  ): Observable<any> {
    let json = {
      "data":
        data
    }

    let Body = JSON.stringify(json); 
    console.log("in predict");
    Body = Body.replace('[', '"')
    Body = Body.replace(']', '"')
    console.log(Body);
    return this.http.post<any>(this.url, Body).pipe(
      map(res => {
        console.log(res);
        let final = res;
        return final
      })
    )
  }

  //Functions
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  strokeCollection: AngularFirestoreCollection;


  public getStroke(userId) {
    this.strokeCollection = this.db.collection(`users/${userId}/stroke`);
    return this.strokeCollection.snapshotChanges()
  }

  
  deleteStroke(userId:string , id:string){
    this.db.doc(`users/${userId}/stroke/${id}`).delete();
  }

  addStroke(
    name:string,
    userId: string, 
    age: number,
    gender: number,
    hypertension: number,
    heart_disease: number,
    ever_married: number,
    work_type: number,
    Residence_type: number,
    avg_glucose_level: number,
    bmi: number,
    smoking_status: number,
    result: string) {
    const stroke: Stroke = {
      name: name, age: age, gender: gender, hypertension: hypertension, heart_disease: heart_disease, ever_married: ever_married, work_type: work_type, Residence_type: Residence_type
      , avg_glucose_level: avg_glucose_level, bmi: bmi, smoking_status: smoking_status, result: result
    };
    this.userCollection.doc(userId).collection(`stroke`).add(stroke);

  }

  constructor(private http: HttpClient, private db: AngularFirestore) { }
}
