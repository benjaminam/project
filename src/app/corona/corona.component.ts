import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';
import { Covid } from '../interfaces/covid';

@Component({
  selector: 'corona',
  templateUrl: './corona.component.html',
  styleUrls: ['./corona.component.css']
})
export class CoronaComponent implements OnInit {
  covids;
  covidsData$:Observable<Covid>;
  hasError:Boolean = false;
  errorMessage:string;
  constructor(private DataService:DataService) { }

  ngOnInit(): void {
    
    this.covidsData$ = this.DataService.getPosts();
    this.covidsData$.subscribe(
      data => {
        this.covids = data;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }
  

}
