import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Restaurant } from '../interfaces/restaurant';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {
  restaurants;
  restaurantsData$:Observable<Restaurant>;
  hasError:Boolean = false;
  errorMessage:string;
  name:string;
  phone_number:string;
  review:string;
  description:string;
  address:string;
  type:string;

  constructor(private RestaurantService:RestaurantService) { }
  
  reloadPage() {
    window.location.reload();
 }
  ngOnInit(): void {
    
    this.restaurantsData$ = this.RestaurantService.getRestaurant();
    this.restaurantsData$.subscribe(
      data => {
        this.restaurants = data;
        this.name=data.name;
        this.phone_number=data.phone_number;
        this.review=data.review;
        this.description=data.description;
        this.address=data.address;
        this.type=data.type;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }
  

}