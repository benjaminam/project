export interface Stroke {
    id?:string,
    name:string,
    age: number,
    gender: number,
    hypertension: number,
    heart_disease: number,
    ever_married: number,
    work_type: number,
    Residence_type: number,
    avg_glucose_level: number,
    bmi: number,
    smoking_status: number,
    result: string
}
