export interface Covid {
        country: string,
        confirmed: number,
        deaths:number,
        recovered: number,
        active: number,
        date: string
}
