export interface Deezer {
    data:[{
        id:number;
        artist: string;
        title:string;
        name:string;
        duration:number;
    }];

}