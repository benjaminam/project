export interface Restaurant {
    name:string;
    type:string;
    description:string;
    review:string;
    phone_number:string;
    address:string;
    hours:string;
    logo:string;
}
