import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Covid } from './interfaces/covid';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private URL = "https://api.covid19api.com/dayone/country/israel";
  constructor(private http: HttpClient) { }

  getPosts():Observable<Covid>{
    return this.http.get<Covid>(this.URL); 
  }}
