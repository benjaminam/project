// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDwqWA_w5ezhbjb1IH0g_GzQ1GyJbe2pYs",
    authDomain: "project-e6dab.firebaseapp.com",
    projectId: "project-e6dab",
    storageBucket: "project-e6dab.appspot.com",
    messagingSenderId: "169362266970",
    appId: "1:169362266970:web:694cc2d677f9a97052f163"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
